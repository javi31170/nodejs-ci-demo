
var express = require('express');
var router = express.Router();



/* GET home page. */
router.get('/', function(req, res, next) {  // eslint-disable-line no-unused-vars
	var resultUserName =  process.env.SECRET_USERNAME || 'Secret username not set' ;  
	var resultPassword = process.env.SECRET_PASSWORD|| 'Secret pasword not set' ;  
	res.render('env', { resultUserName: resultUserName, 
		resultPassword: resultPassword
	});

});

module.exports = router;

